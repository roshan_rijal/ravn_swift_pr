//
//  ChatDetailVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class ChatDetailVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tblChatDetail: UITableView!
    @IBOutlet weak var btnUserDetail: UIButton!
    @IBOutlet weak var btnDots: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var viewSendBox: UIView!
    @IBOutlet weak var viewRecording: UIView!
    @IBOutlet weak var txtMessage: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var lblRecording: UILabel!
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var viewButtonPannel: UIView!
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnImageUpload: UIButton!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        let swipeRight : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ChatDetailVC.swipe(sender:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func swipe(sender: UISwipeGestureRecognizer!) {
        
        switch sender.direction {
        case UISwipeGestureRecognizerDirection.right:
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        default:
            break
        }
    }

    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func btnDotsAction(_ sender: Any) {
    }
    
    @IBAction func btnUserDetailAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_USERDETAILVC, sender: self)
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
    }
    
    @IBAction func btnDismissAction(_ sender: Any) {
    }
    
    @IBAction func btnCameraAction(_ sender: Any) {
    }
    
    @IBAction func btnImageUploadAction(_ sender: Any) {
    }
    
    @IBAction func btnHoldDownAction(_ sender: Any) {
    }
    
    @IBAction func btnHoldReleaseAction(_ sender: Any) {
    }
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods

}
