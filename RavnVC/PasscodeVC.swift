//
//  PasscodeVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class PasscodeVC: UIViewController {

    
    @IBOutlet weak var btnOne: UIButton!
    @IBOutlet weak var btnTwo: UIButton!
    @IBOutlet weak var btnThree: UIButton!
    @IBOutlet weak var btnFour: UIButton!
    @IBOutlet weak var btnFive: UIButton!
    @IBOutlet weak var btnSix: UIButton!
    @IBOutlet weak var btnSeven: UIButton!
    @IBOutlet weak var btnEight: UIButton!
    @IBOutlet weak var btnNine: UIButton!
    @IBOutlet weak var btnZero: UIButton!
    @IBOutlet weak var btnTick: UIButton!
    @IBOutlet weak var btnBackSpace: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblInfo: UILabel!
    
    var userInput:String = ""
    var count:Int = 0

    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    @IBAction func btnOneAction(_ sender: AnyObject) {
        self.userInput.append(String(sender.tag))
        self.txtPassword.text = self.userInput
    }
    
    @IBAction func btnBackSpaceAction(_ sender: AnyObject) {
        if self.userInput.characters.count > 0{
            self.userInput = String(self.userInput.characters.dropLast())
            self.txtPassword.text = self.userInput
        }
    }
    
    @IBAction func btnTickAction(_ sender: Any) {
        if self.txtPassword.text == Settings.getRavnuser().passcode{
            performSegue(withIdentifier: IDENTIFIER_CHATLISTVC, sender: self)
        }else{
            self.txtPassword.text = ""
            self.userInput = ""
            count = count + 1
            if count >= 3 {
                Util.showAlert(title: "Ufff", message: "Stop using other's account", view: self)
            }else{
                Util.showAlert(title: "Wrong Passcode", message: "Please match the passcode you've entered in registration", view: self)
            }
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.txtPassword.text = ""
        self.userInput = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods

}
