//
//  NewsDetailVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController, ViewPopDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    var news:News = News()
    var viewPop:ViewPop = ViewPop()
    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        
    }
    
    fileprivate func loadData() {
        self.lblTitle.text = self.news.title
        self.lblDesc.text = self.news.desc
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func btnPopViewAction(_ sender: AnyObject) {
        self.viewPop = ViewPop.newViewObjectFrame(objectFrame: self.view.frame, theDelegate: self) as! ViewPop
        self.viewPop.frame = self.view.frame
        self.viewPop.center = self.view.center
        self.viewPop.alpha = 0
        self.viewPop.viewDisplay.alpha = 0
        self.view.addSubview(self.viewPop)
        ViewPop.animate(withDuration: ANIMATION_DURATION) {
            self.viewPop.alpha = 1
            self.viewPop.viewDisplay.alpha = 1
        }
    }
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
    //MARK: ViewPop Delegate Methods
    
    func hideButton() {
        ViewPop.animate(withDuration: ANIMATION_DURATION, animations: {
            self.viewPop.alpha = 0
            self.viewPop.viewDisplay.alpha = 0
            }) { (true) in
                self.viewPop.removeFromSuperview()
        }
    }
    
    

}
