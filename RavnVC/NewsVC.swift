//
//  NewsVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class NewsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var arrayOfNews:[News]? = []
    private var selectedNews:News = News()
    
    @IBOutlet weak var btnNewsHeadlines: UIButton!
    @IBOutlet weak var tblNews: UITableView!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        let tap = UILongPressGestureRecognizer(target: self, action: #selector(self.handleFrontTap(gestureRecognizer:)))
        tap.minimumPressDuration = 1.0
        self.btnNewsHeadlines.addGestureRecognizer(tap)
    }
    
    fileprivate func loadData() {
        if Util.isConnectedToInternet(){
            self.showLoading(view: self.view, text: "Loading..")
            APIHandler.sharedInstance.newsFetch(success: { (status, newsArray) in
                self.stopLoading(fromView: self.view)
//                self.arrayOfNews = newsArray as? [News]
                self.arrayOfNews = DatabaseHandler.sharedInstance.getNews()
                self.tblNews.reloadData()
            }) { (message) in
                self.stopLoading(fromView: self.view)
                print(message)
            }
        }else{
            Util.showAlert(title:"Oops", message:"No internet connection..", view:self)
            self.arrayOfNews = DatabaseHandler.sharedInstance.getNews()
            self.tblNews.reloadData()
        }
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    fileprivate func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    
    fileprivate func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    func handleFrontTap(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began{
            performSegue(withIdentifier: IDENTIFIER_PASSCODEVC, sender: self)
        }
    }
        
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewsDetailVC" {
            if self.tblNews.indexPathForSelectedRow != nil {
                let vc = segue.destination as! NewsDetailVC
                vc.news = self.selectedNews
            }
        }
    }
    
    // MARK: -
    // MARK: Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfNews!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CellNews = self.tblNews.dequeueReusableCell(withIdentifier: "CellNews") as! CellNews
        let newsObj:News = self.arrayOfNews![indexPath.row]
        cell.loadCell(news: newsObj)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedNews = self.arrayOfNews![indexPath.row]
        performSegue(withIdentifier: IDENTIFIER_NEWSDETAILVC, sender: self)
    }
    
}
