//
//  PhoneContactVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class PhoneContactVC: UIViewController, UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate {
    let animals: [String] = ["Horse", "Cow", "Camel", "Sheep", "Goat"]

    @IBOutlet weak var tblPhoneContact: UITableView!
    @IBOutlet weak var btnChats: UIButton!
    @IBOutlet weak var viewChats: UIView!
    @IBOutlet weak var btnNewContact: UIButton!
    @IBOutlet weak var viewNewContact: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var btnAdd: UIButton!

    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        self.btnSync.layer.borderColor = UIColor(red: 113/255.0, green: 0/255.0, blue: 253/255.0, alpha: 1.0).cgColor
        self.btnAdd.layer.borderColor = UIColor(red: 113/255.0, green: 0/255.0, blue: 253/255.0, alpha: 1.0).cgColor
        
        let swipeRight : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(PhoneContactVC.swipe(sender:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func swipe(sender: UISwipeGestureRecognizer!) {
        
        switch sender.direction {
        case UISwipeGestureRecognizerDirection.right:
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        default:
            break
        }
    }

    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    @IBAction func btnChatsAction(_ sender: Any) {
        
    }
    
    @IBAction func btnNewContactAction(_ sender: Any) {
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSyncAction(_ sender: Any) {
        
    }
    @IBAction func btnAddAction(_ sender: Any) {
        
    }
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.animals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CellPhoneContact = self.tblPhoneContact.dequeueReusableCell(withIdentifier: "CellPhoneContact") as! CellPhoneContact
        cell.lblName.text = self.animals[indexPath.row]
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

}
