//
//  HomeVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
class HomeVC: UIViewController {

    @IBOutlet weak var btnRegister: UIButton!
    
//    MARK: -
//    MARK: Private Utility Methods
    
    
    private func configureView(){
        
    }
    
    private func loadData() {
        
    }
    
    private func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    
//    MARK: -
//    MARK: Public Utility Methods
    
    
//    MARK: -
//    MARK: IBAction Methods Methods
    @IBAction func touchDown(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnWeatherAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_WEATHERVC, sender: self)
    }
    
    @IBAction func btnFinanceAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_FINANCEVC, sender: self)
    }
    
    @IBAction func btnScoreAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_SPORTVC, sender: self)
    }
    
    @IBAction func btnPictureAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_PICTUREVC, sender: self)
    }
    
    @IBAction func btnNewsAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_NEWSVC, sender: self)
    }
    
    @IBAction func btnPasscodeAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_PASSCODEVC, sender: self)
    }
    
    @IBAction func btnRegisterAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_REGISTRATIONVC, sender: self)
    }
    
//    MARK: -
//    MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        
        if (Util.getCurrentApplicationType() == ApplicationType.ApplicationTypeNews){
            self.btnNewsAction({})
        }else if (Util.getCurrentApplicationType() == ApplicationType.ApplicationTypeSports){
            self.btnScoreAction({})
        }else if (Util.getCurrentApplicationType() == ApplicationType.ApplicationTypeFinance){
            self.btnFinanceAction({})
        }else if (Util.getCurrentApplicationType() == ApplicationType.ApplicationTypePhotos){
            self.btnPictureAction({})
        }else if (Util.getCurrentApplicationType() == ApplicationType.ApplicationTypeWeather){
            self.btnWeatherAction({})
        }else if (Util.getCurrentApplicationType() == ApplicationType.ApplicationTypeRavn){
            self.btnRegisterAction({})
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "") {
            // pass data to next view
        }
    }
    
//    MARK: -
//    MARK: Delegate Methods

}
