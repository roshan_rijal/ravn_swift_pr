//
//  RegistrationVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var btnTermsAndPolicies: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtPasscode: UITextField!
    
    private var ravnUser:RavnUser = RavnUser()
    
    //    MARK: -
    //    MARK: Private Utility Methods
    
    
    private func configureView(){
        self.btnRegister.layer.borderColor = UIColor(red: 113/255.0, green: 0/255.0, blue: 253/255.0, alpha: 1.0).cgColor
    }
    
    
    private func loadData() {
        
    }
    
    private func isValid()->Bool {
        if self.txtUsername.text == ""{
            self.txtUsername.attributedPlaceholder = NSAttributedString(string: "**",attributes: [NSForegroundColorAttributeName: UIColor.red])
            return false
        }else{
            self.txtUsername.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        }
        if self.txtPasscode.text == ""{
            self.txtPasscode.attributedPlaceholder = NSAttributedString(string: "**",attributes: [NSForegroundColorAttributeName: UIColor.red])
            return false
        }else{
            self.txtPasscode.attributedPlaceholder = NSAttributedString(string: "",attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        }
        
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    //    MARK: -
    //    MARK: Public Utility Methods
    
    
    //    MARK: -
    //    MARK: IBAction Methods Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func btnTermsAndPoliciesAction(_ sender: Any) {
//        performSegue(withIdentifier: IDENTIFIER_TERMSANDPOLICIESVC, sender: self)

//        let myVar:RavnUser = RavnUser(username:self.txtName.text!, passcode:self.txtPhone.text!)
//        print("unameR: \(myVar.username) passR: \(myVar.passcode)")
//        let myVC = storyboard?.instantiateViewController(withIdentifier: IDENTIFIER_TERMSANDPOLICIESVC) as! TermsAndPoliciesVC
//        myVC.ravnUser = myVar
//        navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func btnRegisterAction(_ sender: Any){
        if isValid(){
            self.ravnUser.username = self.txtUsername.text!
            self.ravnUser.passcode = self.txtPasscode.text!
            self.ravnUser.language = "en"
            weak var weakSelf = self
            self.showLoading(view: self.view, text: "Registering..")
            RavnHandler.sharedInstance.registerUser(ravnUser: self.ravnUser, success: { (status, message) in
                self.stopLoading(fromView: self.view)
                if status{
                    let vc = weakSelf?.storyboard?.instantiateViewController(withIdentifier: IDENTIFIER_PASSCODEVC) as! PasscodeVC
                    weakSelf?.navigationController?.pushViewController(vc, animated: true)
                }else{
                    Util.showAlert(title: "Oops", message: message, view: self)
                }
                
                
            }) { (message) in
                self.stopLoading(fromView: self.view)
                Util.showAlert(title: "Error", message: message, view: self)
            }
            //        let vc = storyboard?.instantiateViewController(withIdentifier: IDENTIFIER_PASSCODEVC) as! PasscodeVC
            //        navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //    MARK: -
    //    MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
        self.showLoading(view: self.view, text: "loading")
        self.stopLoading(fromView: self.view);
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.txtUsername.text = ""
        self.txtPasscode.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        if Settings.getRavnuser().passcode != "" {
            let vc = storyboard?.instantiateViewController(withIdentifier: IDENTIFIER_PASSCODEVC) as! PasscodeVC
            navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "") {
            // pass data to next view
        }
    }
    
    //    MARK: -
    //    MARK: Delegate Methods

}
