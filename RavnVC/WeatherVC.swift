//
//  WeatherVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

class WeatherVC: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var lblLocationToday: UILabel!
    @IBOutlet weak var lblConditionToday: UILabel!
    @IBOutlet weak var lblTemperatureToday: UILabel!
    @IBOutlet weak var lblTimeToday: UILabel!
    @IBOutlet weak var imgIconToday: UIImageView!
    @IBOutlet weak var lblDayToday: UILabel!
    
    @IBOutlet weak var lblDayOneWeek: UILabel!
    @IBOutlet weak var lblDateOneWeek: UILabel!
    @IBOutlet weak var lblConditionOneWeek: UILabel!
    @IBOutlet weak var lblTemperatureOneWeek: UILabel!
    @IBOutlet weak var imgIconOneWeek: UIImageView!
    
    @IBOutlet weak var lblDayTwoWeek: UILabel!
    @IBOutlet weak var lblDateTwoWeek: UILabel!
    @IBOutlet weak var lblConditionTwoWeek: UILabel!
    @IBOutlet weak var lblTemperatureTwoWeek: UILabel!
    @IBOutlet weak var imgIconTwoWeek: UIImageView!
    
    @IBOutlet weak var lblDayThreeWeek: UILabel!
    @IBOutlet weak var lblDateThreeWeek: UILabel!
    @IBOutlet weak var lblConditionThreeWeek: UILabel!
    @IBOutlet weak var lblTemperatureThreeWeek: UILabel!
    @IBOutlet weak var imgIconThreeWeek: UIImageView!
    
    @IBOutlet weak var lblDayFourWeek: UILabel!
    @IBOutlet weak var lblDateFourWeek: UILabel!
    @IBOutlet weak var lblConditionFourWeek: UILabel!
    @IBOutlet weak var lblTemperatureFourWeek: UILabel!
    @IBOutlet weak var imgIconFourWeek: UIImageView!
    
    @IBOutlet weak var lblDayFiveWeek: UILabel!
    @IBOutlet weak var lblDateFiveWeek: UILabel!
    @IBOutlet weak var lblConditionFiveWeek: UILabel!
    @IBOutlet weak var lblTemperatureFiveWeek: UILabel!
    @IBOutlet weak var imgIconFiveWeek: UIImageView!
    
    @IBOutlet weak var lblDaySixWeek: UILabel!
    @IBOutlet weak var lblDateSixWeek: UILabel!
    @IBOutlet weak var lblConditionSixWeek: UILabel!
    @IBOutlet weak var lblTemperatureSixWeek: UILabel!
    @IBOutlet weak var imgIconSixWeek: UIImageView!
    
    var coreLocationManager = CLLocationManager()
    var locationManager:LocationManager!

    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
//        Alamofire.request("http://api.fixer.io/latest").responseJSON { response in
//            guard let JSON = response.result.value else{
//                print("Error")
//                return
//            }
//            let dict = JSON as! NSDictionary
//            let base : String = dict["base"] as! String
//            print("Base:", base)
//            let date : String = dict["date"] as! String
//            print("Date:", date)
//            let rateDict = dict["rates"] as! NSDictionary
//            let aud : Float = rateDict["AUD"] as! Float
//            print("AUD:", aud)
//        }
        
//        Alamofire.request("http://api.androidhive.info/contacts/").responseJSON { response in
//            guard let JSON = response.result.value else{
//                print("Error")
//                return
//            }
//            let dict = JSON as! NSDictionary
//            let contact = dict["contacts"] as! NSArray
//            for contactDict in contact as! [[String : Any]]{
//                let id : String = contactDict["id"] as! String
//                let name:String = contactDict["name"] as! String
//                let email : String = contactDict["email"] as! String
//                let address : String = contactDict["address"] as! String
//                let gender : String = contactDict["gender"] as! String
//                print("id:", id)
//                print("Name:", name)
//                print("Email:", email)
//                print("Address:", address)
//                print("Gender:", gender)
//                let phoneDict = contactDict["phone"] as! NSDictionary
//                let mobile : String = phoneDict["mobile"] as! String
//                let home : String = phoneDict["home"] as! String
//                let office : String = phoneDict["office"] as! String
//                print("Mobile:", mobile)
//                print("Home:", home)
//                print("Office:", office)
//            }
//        }
        
//        Alamofire.request("http://maps.googleapis.com/maps/api/geocode/json?address=Mannheim").responseJSON { response in
//            guard let JSON = response.result.value else{
//                print("Error")
//                return
//            }
//            let dict = JSON as! NSDictionary
//            let result = dict["results"] as! NSArray
//            for resultDict in result as! [[String:Any]]{
//                let addressComponentsArray = resultDict["address_components"] as! NSArray
//                for addCompDict in addressComponentsArray as! [[String:Any]]{
//                    let longName:String = addCompDict["long_name"] as! String
//                    let shortName:String = addCompDict["short_name"] as! String
//                    print("longName:", longName)
//                    print("shortName:", shortName)
//                    let typesArray = addCompDict["types"] as! NSArray
//                    for typesString in typesArray{
//                        print("typesString:", typesString)
//                    }
//                }
//            }
//        }


    }
    
//    func test(){
//        {
//            "results" : [
//            {
//            "address_components" : [
//            {"long_name" : "40","short_name" : "40","types" : [ "street_number" ]},
//            {"long_name" : "Oststadt","short_name" : "Oststadt","types" : [ "political", 			"sublocality", "sublocality_level_1" ]}],
//            "formatted_address" : "Friedrichsring 40, 68161 Mannheim, Germany",
//            "geometry" : {
//            "location" : {"lat" : 49.49019149999999,"lng" : 8.4760125},
//            "location_type" : "APPROXIMATE",
//            "viewport" : {
//            "northeast" : {"lat" : 49.49154048029149,"lng" : 8.477361480291503},
//            "southwest" : {"lat" : 49.4888425197085,"lng" : 8.474663519708498}
//            }
//            },
//            "place_id" : "ChIJL-lqDCfMl0cR-tLElRS2WN4",
//            "types" : [ "bakery", "establishment", "food", "point_of_interest", "store" ]
//            }
//            ],
//            "status" : "OK"
//        }
//    }
    
    fileprivate func loadData() {
//        self.showLoading(view: self.view, text: "Loading..")
//        APIHandler.sharedInstance.weatherTodayFetch(success: { (status, weatherToday) in
//            self.stopLoading(fromView: self.view)
//            self.lblLocationToday.text = "Kathmandu, Nepal"
//            self.lblConditionToday.text = weatherToday.condition
//            self.lblDayToday.text = weatherToday.day
//            self.lblTemperatureToday.text = weatherToday.temperature
//            self.lblTimeToday.text = weatherToday.date
//            do {
//                self.imgIconToday.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherToday.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//            }) { (status) in
//                self.stopLoading(fromView: self.view)
//                print("Error")
//        }
//        
//        APIHandler.sharedInstance.weatherWeekFetch(success: { (status, weatherWeekArray) in
//            self.weatherWeekArray = weatherWeekArray as! [WeatherWeek]
//            //Day One
//            let weatherWeekDayOne:WeatherWeek = self.weatherWeekArray[0]
//            self.lblDayOneWeek.text = weatherWeekDayOne.day
//            self.lblDateOneWeek.text = weatherWeekDayOne.date
//            self.lblConditionOneWeek.text = weatherWeekDayOne.condition
//            self.lblTemperatureOneWeek.text = weatherWeekDayOne.temperature
//            do {
//                self.imgIconOneWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayOne.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//            //Day Two
//            let weatherWeekDayTwo:WeatherWeek = self.weatherWeekArray[1]
//            self.lblDayTwoWeek.text = weatherWeekDayTwo.day
//            self.lblDateTwoWeek.text = weatherWeekDayTwo.date
//            self.lblConditionTwoWeek.text = weatherWeekDayTwo.condition
//            self.lblTemperatureTwoWeek.text = weatherWeekDayTwo.temperature
//            do {
//                self.imgIconTwoWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayTwo.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//            //Day Three
//            let weatherWeekDayThree:WeatherWeek = self.weatherWeekArray[2]
//            self.lblDayThreeWeek.text = weatherWeekDayThree.day
//            self.lblDateThreeWeek.text = weatherWeekDayThree.date
//            self.lblConditionThreeWeek.text = weatherWeekDayThree.condition
//            self.lblTemperatureThreeWeek.text = weatherWeekDayThree.temperature
//            do {
//                self.imgIconThreeWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayThree.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//            //Day Four
//            let weatherWeekDayFour:WeatherWeek = self.weatherWeekArray[3]
//            self.lblDayFourWeek.text = weatherWeekDayFour.day
//            self.lblDateFourWeek.text = weatherWeekDayFour.date
//            self.lblConditionFourWeek.text = weatherWeekDayFour.condition
//            self.lblTemperatureFourWeek.text = weatherWeekDayFour.temperature
//            do {
//                self.imgIconFourWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayFour.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//            //Day Five
//            let weatherWeekDayFive:WeatherWeek = self.weatherWeekArray[4]
//            self.lblDayFiveWeek.text = weatherWeekDayFive.day
//            self.lblDateFiveWeek.text = weatherWeekDayFive.date
//            self.lblConditionFiveWeek.text = weatherWeekDayFive.condition
//            self.lblTemperatureFiveWeek.text = weatherWeekDayFive.temperature
//            do {
//                self.imgIconFiveWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayFive.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//            //Day Six
//            let weatherWeekDaySix:WeatherWeek = self.weatherWeekArray[5]
//            self.lblDaySixWeek.text = weatherWeekDaySix.day
//            self.lblDateSixWeek.text = weatherWeekDaySix.date
//            self.lblConditionSixWeek.text = weatherWeekDaySix.condition
//            self.lblTemperatureSixWeek.text = weatherWeekDaySix.temperature
//            do {
//                self.imgIconSixWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDaySix.icon) as! URL) as Data)
//            } catch {
//                print("Error:",error)
//            }
//
//            }) { (status) in
//                print("Fetching Error")
//        }
        
        self.showLoading(view: self.view, text: "Loading..")
        APIHandler.sharedInstance.weatherTodayFetch(success: { (status, weather) in
            APIHandler.sharedInstance.weatherWeekFetch(weather: weather, success: { (status, weather) in
                self.stopLoading(fromView: self.view)
                //Today
                self.lblConditionToday.text = weather.weatherToday.condition
                self.lblDayToday.text = weather.weatherToday.day
                self.lblTemperatureToday.text = weather.weatherToday.temperature
                self.lblTimeToday.text = weather.weatherToday.date
                do {
                    self.imgIconToday.image = try UIImage(data:NSData(contentsOf:NSURL(string:weather.weatherToday.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                //Day One
                let weatherWeekDayOne:WeatherWeek = weather.weatherWeekArray[1]
                self.lblDayOneWeek.text = weatherWeekDayOne.day
                self.lblDateOneWeek.text = weatherWeekDayOne.date
                self.lblConditionOneWeek.text = weatherWeekDayOne.condition
                self.lblTemperatureOneWeek.text = weatherWeekDayOne.temperature
                do {
                    self.imgIconOneWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayOne.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                //Day Two
                let weatherWeekDayTwo:WeatherWeek = weather.weatherWeekArray[2]
                self.lblDayTwoWeek.text = weatherWeekDayTwo.day
                self.lblDateTwoWeek.text = weatherWeekDayTwo.date
                self.lblConditionTwoWeek.text = weatherWeekDayTwo.condition
                self.lblTemperatureTwoWeek.text = weatherWeekDayTwo.temperature
                do {
                    self.imgIconTwoWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayTwo.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                //Day Three
                let weatherWeekDayThree:WeatherWeek = weather.weatherWeekArray[3]
                self.lblDayThreeWeek.text = weatherWeekDayThree.day
                self.lblDateThreeWeek.text = weatherWeekDayThree.date
                self.lblConditionThreeWeek.text = weatherWeekDayThree.condition
                self.lblTemperatureThreeWeek.text = weatherWeekDayThree.temperature
                do {
                    self.imgIconThreeWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayThree.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                //Day Four
                let weatherWeekDayFour:WeatherWeek = weather.weatherWeekArray[4]
                self.lblDayFourWeek.text = weatherWeekDayFour.day
                self.lblDateFourWeek.text = weatherWeekDayFour.date
                self.lblConditionFourWeek.text = weatherWeekDayFour.condition
                self.lblTemperatureFourWeek.text = weatherWeekDayFour.temperature
                do {
                    self.imgIconFourWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayFour.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                //Day Five
                let weatherWeekDayFive:WeatherWeek = weather.weatherWeekArray[5]
                self.lblDayFiveWeek.text = weatherWeekDayFive.day
                self.lblDateFiveWeek.text = weatherWeekDayFive.date
                self.lblConditionFiveWeek.text = weatherWeekDayFive.condition
                self.lblTemperatureFiveWeek.text = weatherWeekDayFive.temperature
                do {
                    self.imgIconFiveWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDayFive.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                //Day Six
                let weatherWeekDaySix:WeatherWeek = weather.weatherWeekArray[6]
                self.lblDaySixWeek.text = weatherWeekDaySix.day
                self.lblDateSixWeek.text = weatherWeekDaySix.date
                self.lblConditionSixWeek.text = weatherWeekDaySix.condition
                self.lblTemperatureSixWeek.text = weatherWeekDaySix.temperature
                do {
                    self.imgIconSixWeek.image = try UIImage(data:NSData(contentsOf:NSURL(string:weatherWeekDaySix.icon) as! URL) as Data)
                } catch {
                    print("Error:",error)
                }
                
                }, failure: { (status) in
                    self.stopLoading(fromView: self.view)
                    print("Error:",status)
            })
            }) { (status) in
                self.stopLoading(fromView: self.view)
                print("Error:",status)
        }
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    func getLocation(){
        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, message, error) in
            Util.setLatitude(latitude: String(format:"%f", latitude))
            Util.setLongitude(longitude: String(format:"%f", longitude))
            print("lati:", String(format:"%f", latitude))
            print("longi:", String(format:"%f", longitude))

            self.locationManager.reverseGeocodeLocationUsingGoogleWithLatLon(latitude: latitude, longitude: longitude) { (reverseGeocodeInfo, placemark, error) -> Void in
                
                let address = reverseGeocodeInfo?.object(forKey: "formattedAddress") as! String
                self.lblLocationToday.text = address
                print("adddd:", address)
            }
        }
        
    }
    
    
    
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coreLocationManager.delegate = self
        locationManager = LocationManager.sharedInstance
        coreLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        coreLocationManager.requestAlwaysAuthorization()
        getLocation()
        self.loadData()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        getLocation()
//        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
//            if error != nil{
//                print("Error")
//            }else{
//                if let place = placemark?[0]{
//                    self.lblLocationToday.text = place.country
//                    print("place:", place.country)
//                }
//            }
//        }
    }
    
}
