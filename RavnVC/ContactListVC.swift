//
//  ContactListVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class ContactListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let animals: [String] = ["Horse", "Cow", "Camel", "Sheep", "Goat"]

    @IBOutlet weak var tblContactList: UITableView!
    @IBOutlet weak var btnDots: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    @IBOutlet weak var btnSettings: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var btnChats: UIButton!
    @IBOutlet weak var viewChats: UIView!
    @IBOutlet weak var btnContacts: UIButton!
    @IBOutlet weak var viewContacts: UIView!
    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        self.btnContacts.setTitleColor(UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0), for: UIControlState.normal)
        self.viewContacts.backgroundColor = UIColor(red: 113/255.0, green: 0/255.0, blue: 253/255.0, alpha: 1.0)
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    @IBAction func btnDotsAction(_ sender: Any) {
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSearchAction(_ sender: Any) {
    }
    
    @IBAction func btnAddAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_PHONECONTACTVC, sender: self)
    }
    
    @IBAction func btnCameraAction(_ sender: Any) {
    }
    @IBAction func btnGalleryAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_GALLERYVC, sender: self)
    }
    
    @IBAction func btnSettingsVC(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_SETTINGSVC, sender: self)
    }

    @IBAction func btnExitAction(_ sender: Any) {
        self.navigationController?.backToViewController(viewController: PasscodeVC.self)
    }

    @IBAction func btnChatsAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: false)
        }
    }

    @IBAction func btnContactsAction(_ sender: Any) {
    }
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.animals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:CellContactList = self.tblContactList.dequeueReusableCell(withIdentifier: "CellContactList") as! CellContactList
        cell.lblName.text = self.animals[indexPath.row]
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: IDENTIFIER_CHATDETAILVC, sender: self)
    }

}

extension UINavigationController {
    func backToViewController(viewController: Swift.AnyClass) {
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
