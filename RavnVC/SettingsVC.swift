//
//  SettingsVC.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/9/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController, UIGestureRecognizerDelegate{

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var imgProfileImage: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var btnUsername: UIButton!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var btnPhoneNumber: UIButton!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var btnLanguage: UIButton!
    @IBOutlet weak var lblFont: UILabel!
    @IBOutlet weak var btnFont: UIButton!
    @IBOutlet weak var lblPasscode: UILabel!
    @IBOutlet weak var btnPasscode: UIButton!
    @IBOutlet weak var notfnOnOff: UISegmentedControl!
    @IBOutlet weak var notfnSoundOnOff: UISegmentedControl!
    @IBOutlet weak var notfnLightOnOff: UISegmentedControl!
    @IBOutlet weak var btnAdvancedCustomNotfn: UIButton!
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var btnTutorial: UIButton!
    @IBOutlet weak var btnClearRecord: UIButton!
    @IBOutlet weak var btnDeactivateAccount: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    
    // MARK: -
    // MARK: Private Utility Methods
    
    
    fileprivate func configureView(){
        let swipeRight : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(SettingsVC.swipe(sender:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)

    }
    
    func swipe(sender: UISwipeGestureRecognizer!) {
        switch sender.direction {
        case UISwipeGestureRecognizerDirection.right:
            if let navController = self.navigationController {
                navController.popViewController(animated: true)
            }
        default:
            break
        }
    }
    
    fileprivate func loadData() {
        
    }
    
    fileprivate func isValid()->Bool {
        return true
    }
    
    private func showLoading(view:UIView, text:String){
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = text
    }
    
    private func stopLoading(fromView:UIView){
        MBProgressHUD.hide(for: fromView, animated: true)
    }
    
    // MARK: -
    // MARK: Public Utility Methods
    
    
    // MARK: -
    // MARK: IBAction Methods Methods
    
    @IBAction func btnBackAction(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    @IBAction func btnCameraAction(_ sender: Any) {
        
    }
    @IBAction func btnUsernameAction(_ sender: Any) {
        
    }
    
    @IBAction func btnPhoneNumberAction(_ sender: Any) {
        
    }
    
    @IBAction func btnLanguageAction(_ sender: Any) {
        
    }
    
    @IBAction func btnFontAction(_ sender: Any) {
        
    }
    
    @IBAction func btnPasscodeAction(_ sender: Any) {
        
    }

    @IBAction func btnAdvancedCustomNotfnAction(_ sender: Any) {
        
    }
    
    @IBAction func btnHelpAction(_ sender: Any) {
        performSegue(withIdentifier: IDENTIFIER_HELPVC, sender: self)
    }

    @IBAction func btnTutorialAction(_ sender: Any) {
        
    }
    
    @IBAction func btnClearRecordAction(_ sender: Any) {
        
    }

    @IBAction func btnDeactivateAccountAction(_ sender: Any) {
        let ravnUser:RavnUser = RavnUser()
        ravnUser.username = Settings.getRavnuser().username
        ravnUser.language = "en"
        let alert = UIAlertController(title: "Deactivate Account", message: "Wanna leave Ravn?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.showLoading(view: self.view, text: "Deactivating..")
            RavnHandler.sharedInstance.unregisterUser(ravnUser: ravnUser, success: { (status) in
                self.stopLoading(fromView: self.view)
                Settings.resetRavnUser()
                self.navigationController?.backToRegistration(viewController: RegistrationVC.self)
            }) { (message) in
                self.stopLoading(fromView: self.view)
                Util.showAlert(title: "Oops", message: "Deactivate failed", view: self)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            //
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
    }
    
    
    // MARK: -
    // MARK: Object Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: -
    // MARK: Delegate Methods

}

extension UINavigationController {
    func backToRegistration(viewController: Swift.AnyClass) {
        for element in viewControllers as Array {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}
