//
//  RavnHandler.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/15/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class RavnHandler: NSObject {

    static let sharedInstance = RavnHandler()

    func registerUser(ravnUser:RavnUser, success:@escaping (_ status:Bool, _ message:String) -> (), failure: @escaping (_ message:String) -> ()){
        let parametersDictionary = [
            "language" : ravnUser.language,
            "userid" : ravnUser.username,
            "primary_passcode" : ravnUser.passcode
        ]
        Alamofire.request(REGISTRATION_URL, method: .post, parameters: (parametersDictionary as NSDictionary) as? Parameters , encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                        if let JSON = response.result.value {
                            print("JSON", JSON)
                            let dict = JSON as! NSDictionary
                            let status : Bool = dict["status"] as! Bool
                            let message : String = dict["msg"] as! String
                            if(status){
                                Settings.setRavnuser(ravnUser: ravnUser)
                                success(status, message)
                            }else{
                                failure(message)
                            }
                        }
            }
    }
    
    func unregisterUser(ravnUser:RavnUser, success:@escaping (_ status:Bool) -> (), failure:@escaping (_ message:String) -> ()){
        let parametersDictionary = [
            "userid" : ravnUser.username,
            "language" : ravnUser.language
        ]
        Alamofire.request(UNREGISTRATION_URL, method: .post, parameters: (parametersDictionary as NSDictionary) as? Parameters , encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            if let JSON = response.result.value {
                print("JSON", JSON)
                let dict = JSON as! NSDictionary
                let status : Bool = dict["status"] as! Bool
                if(status){
                    success(status)
                }else{
                    failure("Deactivate error")
                }
            }
        }
    }
    
}
