//
//  APIHandler.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/15/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire

class APIHandler: NSObject {

    static let sharedInstance = APIHandler()
        
    func newsFetch(success:@escaping (_ status:Bool, _ newsArray:Any) -> (), failure:@escaping (_ message: Error) -> ()) {
        Alamofire.request(NEWS_FEED_URL).responseJSON { response in
//            print("Request: \(response.request)")  // original URL request
//            print("Response\(response.response)") // HTTP URL response
//            print("Data:\(response.data)")     // server data
//            print("Result: \(response.result)")   // result of response serialization
/************************************************************************************************/
            guard let newsResponse = response.result.value as? [[String:Any]] else{
                print("Error: \(response.result.error)")
                failure((response.result.error! as Error))
                return
            }
//            print("JSON: \(newsResponse)")
            DatabaseHandler.sharedInstance.clearNews()
            var newsArray:Array = [News]()
            for dict in newsResponse{
                if let title = dict["title"],
                    let desc = dict["description"],
                    let date = dict["pubDate"],
                    let imageUrl = dict["image"]
                {
                    let newsObj:News = News()
                    newsObj.title = title as! String
                    newsObj.desc = desc as! String
                    newsObj.date = date as! String
                    newsObj.imageUrl = "http:\(imageUrl)"
                    newsArray.append(newsObj)
                    DatabaseHandler.sharedInstance.createNews(newsObj: newsObj)
                }
            }
            success(true, newsArray)
        }
    }
    
//    func weatherTodayFetch(success:@escaping (_ status:Bool, _ weatherToday:WeatherToday) -> (), failure:@escaping (_ message: Error) -> ()) {
//        let weatherTodayUrl = "http://app.ravnapp.com/ciravn/v2/weather?lat=27.7172&lon=85.3240&isToday=1"
//        Alamofire.request(weatherTodayUrl).responseJSON { response in
//            guard let weatherTodayResponse = response.result.value as? [[String:Any]] else{
//                print("Error: \(response.result.error)")
//                failure((response.result.error! as Error))
//                return
//            }
//            //            print("JSON: \(weatherTodayResponse)")
//            let weatherTodayObj:WeatherToday = WeatherToday()
//            for dict in weatherTodayResponse{
//                if let date = dict["date"],
//                    let day = dict["day"],
//                    let condition = dict["condition"],
//                    let icon = dict["icon"],
//                    let temperature = dict["temperature"]
//                {
//                    weatherTodayObj.date = date as! String
//                    weatherTodayObj.day = day as! String
//                    weatherTodayObj.condition = condition as! String
//                    weatherTodayObj.icon = icon as! String
//                    weatherTodayObj.temperature = temperature as! String
//                }
//            }
//            success(true, weatherTodayObj)
//        }
//    }
//    
//    func weatherWeekFetch(success:@escaping (_ status:Bool, _ weatherWeekArray:Any) -> (), failure:@escaping (_ message: Error) -> ()) {
//        let weatherWeekUrl = "http://app.ravnapp.com/ciravn/v2/weather?lat=27.7172&lon=85.3240"
//        Alamofire.request(weatherWeekUrl).responseJSON { response in
//            guard let weatherWeekResponse = response.result.value as? [[[String:Any]]] else{
//                print("Error: \(response.result.error)")
//                failure((response.result.error! as Error))
//                return
//            }
////                        print("JSON: \(weatherWeekResponse)")
//            var weatherWeekArray:Array = [WeatherWeek]()
//            for innerArray in weatherWeekResponse{
//                for dict in innerArray{
//                    if let date = dict["date"],
//                        let day = dict["day"],
//                        let condition = dict["condition"],
//                        let icon = dict["icon"],
//                        let temperature = dict["temperature"]
//                    {
//                        let weatherWeekObj:WeatherWeek = WeatherWeek()
//                        weatherWeekObj.date = date as! String
//                        weatherWeekObj.day = day as! String
//                        weatherWeekObj.condition = condition as! String
//                        weatherWeekObj.icon = icon as! String
//                        weatherWeekObj.temperature = temperature as! String
//                        weatherWeekArray.append(weatherWeekObj)
//                    }
//                }
//            }
//            success(true, weatherWeekArray)
//        }
//    }
    
    
    func weatherTodayFetch(success:@escaping (_ status:Bool, _ weather:Weather) -> (), failure:@escaping (_ message: Error) -> ()) {
//        let weatherTodayUrl = "http://app.ravnapp.com/ciravn/v2/weather?lat=27.7172&lon=85.3240&isToday=1"
        let weatherTodayUrl = "http://app.ravnapp.com/ciravn/v2/weather?lat=\(Util.latitude())&lon=\(Util.longitude())&isToday=1"
        Alamofire.request(weatherTodayUrl).responseJSON { response in
            guard let weatherTodayResponse = response.result.value as? [[String:Any]] else{
                print("Error: \(response.result.error)")
                failure((response.result.error! as Error))
                return
            }
            //            print("JSON: \(weatherTodayResponse)")
            for dict in weatherTodayResponse{
                if let date = dict["date"],
                    let day = dict["day"],
                    let condition = dict["condition"],
                    let icon = dict["icon"],
                    let temperature = dict["temperature"]
                {
                    let weather:Weather = Weather()
                    weather.weatherToday.date = date as! String
                    weather.weatherToday.day = day as! String
                    weather.weatherToday.condition = condition as! String
                    weather.weatherToday.icon = icon as! String
                    weather.weatherToday.temperature = temperature as! String
                    success(true, weather)
                }
            }
        }
    }
    
    func weatherWeekFetch(weather:Weather, success:@escaping (_ status:Bool, _ weather:Weather) -> (), failure:@escaping (_ message: Error) -> ()) {
//        let weatherWeekUrl = "http://app.ravnapp.com/ciravn/v2/weather?lat=27.7172&lon=85.3240"
        let weatherWeekUrl = "http://app.ravnapp.com/ciravn/v2/weather?lat=\(Util.latitude())&lon=\(Util.longitude())"
        Alamofire.request(weatherWeekUrl).responseJSON { response in
            guard let weatherWeekResponse = response.result.value as? [[[String:Any]]] else{
                print("Error: \(response.result.error)")
                failure((response.result.error! as Error))
                return
            }
            //        print("JSON: \(weatherWeekResponse)")
            for innerArray in weatherWeekResponse{
                for dict in innerArray{
                    if let date = dict["date"],
                        let day = dict["day"],
                        let condition = dict["condition"],
                        let icon = dict["icon"],
                        let temperature = dict["temperature"]
                    {
                        let weatherWeek:WeatherWeek = WeatherWeek()
                        weatherWeek.date = date as! String
                        weatherWeek.day = day as! String
                        weatherWeek.condition = condition as! String
                        weatherWeek.icon = icon as! String
                        weatherWeek.temperature = temperature as! String
                        weather.weatherWeekArray.append(weatherWeek)
                    }
                }
            }
            success(true, weather)
        }
    }

    
}
