//
//  DatabaseHandler.swift
//  Ravn_Swift
//
//  Created by Roshan on 5/6/17.
//  Copyright © 2017 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData

class DatabaseHandler: NSObject {
    static let sharedInstance = DatabaseHandler()
    
    var managedObjectContext:NSManagedObjectContext!
    
    func createNews(newsObj:News){
        managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let dbNews = DBNews(context: managedObjectContext)
        dbNews.title = newsObj.title
        dbNews.desc = newsObj.desc
        dbNews.date = newsObj.date
        dbNews.imageUrl = newsObj.imageUrl
        do{
            try self.managedObjectContext.save()
        }
        catch{
            print("Save failed.\(error.localizedDescription)")
        }
    }
    
    
    
    func getNews() -> [News]{
        managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        var dbNewsArray = [News]()
        let newsRequest:NSFetchRequest<DBNews> = DBNews.fetchRequest()
        do{
            let data = try managedObjectContext.fetch(newsRequest)
            for dbNews in data{
                let newsObj:News = News()
                newsObj.title = (dbNews as AnyObject).title!
                newsObj.desc = (dbNews as AnyObject).desc!
                newsObj.date = (dbNews as AnyObject).date!
                newsObj.imageUrl = (dbNews as AnyObject).imageUrl!
                dbNewsArray.append(newsObj)
            }
        }
        catch{
            print("Fetch failed\(error.localizedDescription)")
        }
        return dbNewsArray
    }
    
    func clearNews(){
        managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let deleteNews = NSFetchRequest<NSFetchRequestResult>(entityName: "DBNews")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteNews)
        do {
            try managedObjectContext.execute(deleteRequest)
            try managedObjectContext.save()
        } catch {
            print ("There was an error\(error.localizedDescription)")
        }
    }

}
