//
//  Util.swift
//  ResearchiOS
//
//  Created by Roshan on 7/8/16.
//  Copyright © 2016 Roshan. All rights reserved.
//

import Foundation
import Alamofire

var latitudeNew:String = "27.7172"
var longitudeNew:String = "85.3240"


class Util {
    
    class func getCurrentApplicationType()->ApplicationType {
        let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
        var  applicationType: ApplicationType = ApplicationType.ApplicationTypeRavn
        if appName == BUNDLE_NAME_RAVN {
            applicationType = ApplicationType.ApplicationTypeRavn
        } else if appName == BUNDLE_NAME_NEWS {
            applicationType = ApplicationType.ApplicationTypeNews
        } else if appName == BUNDLE_NAME_SPORTS {
            applicationType = ApplicationType.ApplicationTypeSports
        } else if appName == BUNDLE_NAME_FINANCE {
            applicationType = ApplicationType.ApplicationTypeFinance
        } else if appName == BUNDLE_NAME_PHOTOS {
            applicationType = ApplicationType.ApplicationTypePhotos
        } else if appName == BUNDLE_NAME_WEATHER {
            applicationType = ApplicationType.ApplicationTypeWeather
        }
        return applicationType
    }
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func showAlert(title:String, message:String, view:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
    
    class func resetAsAppRunningFirstTime(){
        if(UserDefaults.standard.bool(forKey: "HasLaunchedOnce")){
            // app already launched
        }else{
            // This is the first launch ever
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
        }
    }
    
    class func setLatitude(latitude:String){
        latitudeNew = latitude
    }
    
    class func setLongitude(longitude:String){
        longitudeNew = longitude
        
    }
    
    class func latitude() -> String{
        return latitudeNew
    }
    
    class func longitude() -> String{
        return longitudeNew
    }

}

