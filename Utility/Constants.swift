//
//  Constants.swift
//  ResearchiOS
//
//  Created by Roshan on 7/8/16.
//  Copyright © 2016 Roshan. All rights reserved.
//

import Foundation

/***************************************************************************************************/
/*Sekeleton For ViewController*/

//// MARK: -
//// MARK: Private Utility Methods
//
//
//fileprivate func configureView(){
//
//}
//
//fileprivate func loadData() {
//
//}
//
//fileprivate func isValid()->Bool {
//    return true
//}
//
//private func showLoading(view:UIView, text:String){
//    let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
//    loadingNotification.mode = MBProgressHUDMode.indeterminate
//    loadingNotification.label.text = text
//}
//
//private func stopLoading(fromView:UIView){
//    MBProgressHUD.hide(for: fromView, animated: true)
//}
//
//// MARK: -
//// MARK: Public Utility Methods
//
//
//// MARK: -
//// MARK: IBAction Methods Methods
//
//
//// MARK: -
//// MARK: Object Methods
//
//override func awakeFromNib() {
//    super.awakeFromNib()
//}
//
//override func loadView() {
//    super.loadView()
//}
//
//override func viewDidLayoutSubviews() {
//    super.viewDidLayoutSubviews()
//}
//
//override func viewDidLoad() {
//    super.viewDidLoad()
//}
//
//override func viewWillAppear(_ animated: Bool) {
//    super.viewWillAppear(animated)
//}
//
//override func viewDidAppear(_ animated: Bool) {
//    super.viewDidAppear(animated);
//}
//
//override func viewWillDisappear(_ animated: Bool) {
//    super.viewWillDisappear(animated)
//}
//
//override func viewDidDisappear(_ animated: Bool) {
//    super.viewDidDisappear(animated)
//}
//
//override func didReceiveMemoryWarning() {
//    super.didReceiveMemoryWarning()
//    // Dispose of any resources that can be recreated.
//}
//
//override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//}
//
//// MARK: -
//// MARK: Delegate Methods

/***************************************************************************************************/

// MARK:-
// MARK:- Private Utility Methods

// MARK:-
// MARK:- Public Utility Methods

// MARK:-
// MARK:- IBAction Methods

// MARK:-
// MARK:- Object Methods

// MARK:-
// MARK:- Delegate Methods

enum ApplicationType: Int{
    case ApplicationTypeRavn = 0
    case ApplicationTypeNews
    case ApplicationTypeSports
    case ApplicationTypeFinance
    case ApplicationTypePhotos
    case ApplicationTypeWeather
}

let BUNDLE_NAME_RAVN                                = "Ravn"
let BUNDLE_NAME_NEWS                                = "News Headlines"
let BUNDLE_NAME_SPORTS                              = "Sports Headlines"
let BUNDLE_NAME_FINANCE                             = "Finance Headlines"
let BUNDLE_NAME_PHOTOS                              = "Daily Photos"
let BUNDLE_NAME_WEATHER                             = "Local Weather"

let IDENTIFIER_WEATHERVC                            = "WeatherVC"
let IDENTIFIER_FINANCEVC                            = "FinanceVC"
let IDENTIFIER_SPORTVC                              = "SportVC"
let IDENTIFIER_PICTUREVC                            = "PictureVC"
let IDENTIFIER_NEWSVC                               = "NewsVC"
let IDENTIFIER_PASSCODEVC                           = "PasscodeVC"
let IDENTIFIER_REGISTRATIONVC                       = "RegistrationVC"
let IDENTIFIER_CHATLISTVC                           = "ChatListVC"
let IDENTIFIER_CONTACTLISTVC                        = "ContactListVC"
let IDENTIFIER_CHATDETAILVC                         = "ChatDetailVC"
let IDENTIFIER_SETTINGSVC                           = "SettingsVC"
let IDENTIFIER_GALLERYVC                            = "GalleryVC"
let IDENTIFIER_REGISTRATIONVERICATIONVC             = "RegistrationVerificationVC"
let IDENTIFIER_TERMSANDPOLICIESVC                   = "TermsAndPoliciesVC"
let IDENTIFIER_NEWSDETAILVC                         = "NewsDetailVC"
let IDENTIFIER_USERDETAILVC                         = "UserDetailVC"
let IDENTIFIER_HELPVC                               = "HelpVC"
let IDENTIFIER_PHONECONTACTVC                       = "PhoneContactVC"


let NEWS_FEED_URL                                   = "http://app.ravnapp.com/ciravn/v2/news/index/News"
let REGISTRATION_URL                                = "http://app.ravnapp.com/ciravn/v2/users/register"
let UNREGISTRATION_URL                              = "http://52.5.92.124/ciravn/v2/users/unregister"
let WEATHER_TODAY_URL                               = "http://app.ravnapp.com/ciravn/v2/weather?lat=%@&lon=%@&isToday=1"
let WEATHER_WEEK_URL                                = "http://app.ravnapp.com/ciravn/v2/weather?lat=%@&lon=%@"

let ANIMATION_DURATION                              = 0.5




//let UIColorWhiteOff = UIColor(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1)

