//
//  ViewPop.swift
//  Ravn_Swift
//
//  Created by Roshan on 5/10/17.
//  Copyright © 2017 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

protocol ViewPopDelegate {
    func hideButton()
}

class ViewPop: UIControl {

    @IBOutlet weak var btnHide: UIButton!
    @IBOutlet weak var viewDisplay: UIView!
    
//    var viewPop:ViewPop = ViewPop()
    
    // MARK:- Delegate
    var delegate:ViewPopDelegate?
    
    // MARK:-
    // MARK:- Private Utility Methods
    
    // MARK:-
    // MARK:- Object Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    // MARK:-
    // MARK:- IBAction Methods
    @IBAction func touchDown(_ sender: AnyObject){
        if self.delegate != nil{
            self.delegate?.hideButton()
        }
    }
    
    @IBAction func btnHideAction(_ sender: AnyObject) {
        if self.delegate != nil{
            self.delegate?.hideButton()
        }
    }
    
    // MARK:-
    // MARK:- Public Utility Methods
    
    class func newViewObjectFrame(objectFrame:CGRect, theDelegate:ViewPopDelegate) -> UIView{
        let nib = UINib(nibName: "ViewPop", bundle: nil)
        let nibArray:Array = nib.instantiate(withOwner: self, options: nil)
        let viewPop:ViewPop = nibArray[0] as! ViewPop
        viewPop.frame = objectFrame
        viewPop.delegate = theDelegate
        viewPop.tag = 10
        return viewPop
    }
    
    
    // MARK:-
    // MARK:- Delegate Methods
    
}

