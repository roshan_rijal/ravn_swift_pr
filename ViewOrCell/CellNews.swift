//
//  CellNews.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/15/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit
import SDWebImage

class CellNews: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgPicture: UIImageView!
    
    func loadCell(news:News){
        self.lblTitle.text = news.title
        self.lblDesc.text = news.desc
        self.lblDate.text = news.date
        self.imgPicture.sd_setImage(with: URL(string:news.imageUrl))
//        do {
//            self.imgPicture.image = try UIImage(data:NSData(contentsOf:NSURL(string:news.imageUrl) as! URL) as Data)
//        } catch {
//            print("Error:",error)
//        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
