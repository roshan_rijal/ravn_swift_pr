//
//  CellCollectionGallery.swift
//  Ravn_Swift
//
//  Created by Roshan on 12/2/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class CellCollectionGallery: UICollectionViewCell {
    
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
}
