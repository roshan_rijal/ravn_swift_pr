//
//  Weather.swift
//  Ravn_Swift
//
//  Created by Roshan on 5/16/17.
//  Copyright © 2017 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class Weather: NSObject {
    var weatherToday:WeatherToday = WeatherToday()
    var weatherWeek:WeatherWeek = WeatherWeek()
    
    var weatherWeekArray:[WeatherWeek] = []
    
    override var description:String {
        return "date :\(self.weatherWeek.date) \n day \(self.weatherWeek.day)"
    }
}
