//
//  WeatherToday.swift
//  Ravn_Swift
//
//  Created by Roshan on 5/13/17.
//  Copyright © 2017 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class WeatherToday: NSObject {

    var date:String = ""
    var day:String = ""
    var condition:String = ""
    var icon:String = ""
    var temperature:String = ""
}


