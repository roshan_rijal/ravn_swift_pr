//
//  News.swift
//  Ravn_Swift
//
//  Created by Roshan on 4/28/17.
//  Copyright © 2017 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

class News: NSObject {
    var title: String = ""
    var desc: String = ""
    var date:String = ""
    var imageUrl:String = ""
}
