//
//  Settings.swift
//  Ravn_Swift
//
//  Created by Roshan on 11/16/16.
//  Copyright © 2016 Chronelab Technology Pvt. Ltd. All rights reserved.
//

import UIKit

enum ravnRegistrationKeys : String {
    case username
    case passcode
}

class Settings: NSObject {
    
    class func setRavnuser(ravnUser:RavnUser){
        UserDefaults.standard.setValue(ravnUser.username, forKey: ravnRegistrationKeys.username.rawValue)
        UserDefaults.standard.setValue(ravnUser.passcode, forKey: ravnRegistrationKeys.passcode.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getRavnuser() -> (RavnUser){
        let ravnUser : RavnUser = RavnUser()
        if UserDefaults.standard.object(forKey: ravnRegistrationKeys.username.rawValue) != nil{
            ravnUser.username = UserDefaults.standard.object(forKey: ravnRegistrationKeys.username.rawValue) as! String
        }
        if UserDefaults.standard.object(forKey: ravnRegistrationKeys.passcode.rawValue) != nil{
            ravnUser.passcode = UserDefaults.standard.object(forKey: ravnRegistrationKeys.passcode.rawValue) as! String
        }
        return ravnUser
    }
    
    class func resetRavnUser(){
        UserDefaults.standard.removeObject(forKey: ravnRegistrationKeys.username.rawValue)
        UserDefaults.standard.removeObject(forKey: ravnRegistrationKeys.passcode.rawValue)
    }
}
